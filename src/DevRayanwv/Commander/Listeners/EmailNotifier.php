<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 5:00 PM
 */

namespace DevRayanwv\Commander\Listeners;


use DevRayanwv\Commander\Eventing\EventListener;
use DevRayanwv\Commander\Customer\CustomerWasAdded;
use DevRayanwv\Commander\Customer\CustomerWasDeleted;


class EmailNotifier extends EventListener  {

    public function whenCustomerWasAdded(CustomerWasAdded $event){
        var_dump('Send an email about event '. $event->customer->email);
    }

    public function whenCustomerWasDeleted(CustomerWasDeleted $event){
        var_dump('The customer deleted '. $event->customer->name);
    }
}