<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 03/06/15
 * Time: 4:56 PM
 */

namespace DevRayanwv\Commander;

use Response;

class ApiResponse {

    protected $statusCode = 200;


    /**
     * @return int
     */

    //TODO handle exceptions
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function respond($data, $headers = []){
        return Response::json($data, $this->getStatusCode(), $headers);

    }

    public function respondWithError($message){
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

    public function respondWithSuccess($message){
        return $this->respond([
            'success' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }


    public function respondNotFound($message = 'Not Found'){
        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondCreated($message = 'Created'){
        return $this->setStatusCode(201)->respondWithSuccess($message);
    }

}