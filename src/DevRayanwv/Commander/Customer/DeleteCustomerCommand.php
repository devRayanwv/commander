<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 5:40 PM
 */

namespace DevRayanwv\Commander\Customer;


class DeleteCustomerCommand {
    public $customerId;

    function __construct($customerId)
    {
        $this->customerId = $customerId;
    }

}