<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 11:31 AM
 */

namespace DevRayanwv\Commander\Customer;


use DevRayanwv\Commander\Eventing\EventGenerator;

class Customer extends \Eloquent {

    use EventGenerator;
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password'];

    public static function add($name, $email, $password){

        $customer = static::create(compact('name', 'email', 'password'));

        $customer->raise(new CustomerWasAdded($customer));

        return $customer;
    }

    public function del(){
        $this->delete();
        $this->raise(new CustomerWasDeleted($this));
    }
}