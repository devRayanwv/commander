<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 11:16 AM
 */

namespace DevRayanwv\Commander\Customer;


use DevRayanwv\Commander\Commanding\CommandHandler;
use DevRayanwv\Commander\Eventing\EventDispatcher;

class AddCustomerCommandHandler implements CommandHandler{

    protected $dispatcher;

    function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function handle($command){

        $customer = Customer::add($command->name, $command->email, $command->password);

        $this->dispatcher->dispatch($customer->releaseEvents());
    }
}