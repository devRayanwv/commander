<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 5:41 PM
 */

namespace DevRayanwv\Commander\Customer;


use DevRayanwv\Commander\Commanding\CommandHandler;
use DevRayanwv\Commander\Eventing\EventDispatcher;

class DeleteCustomerCommandHandler implements CommandHandler {

    protected $customer;
    protected $dispatcher;
    function __construct(Customer $customer, EventDispatcher $dispatcher)
    {
        $this->customer = $customer;
        $this->dispatcher = $dispatcher;
    }

    public function handle($command)
    {
        $customer = $this->customer->findOrFail($command->customerId);
        $customer->del();

        $this->dispatcher->dispatch($customer->releaseEvents());
    }

}