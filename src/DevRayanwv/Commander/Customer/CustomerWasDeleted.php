<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 5:51 PM
 */

namespace DevRayanwv\Commander\Customer;


class CustomerWasDeleted {
    public $customer;

    function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

}