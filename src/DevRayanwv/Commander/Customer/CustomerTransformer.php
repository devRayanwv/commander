<?php namespace DevRayanwv\Commander\Customer;
use DevRayanwv\Commander\Transforms\Transformer;

/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 21/05/15
 * Time: 11:34 AM
 */


class CustomerTransformer extends Transformer {
    public function transform($customer){
        return [
            'id' => $customer['id'],
            'name' => $customer['name'],
            'email' => $customer['email']
        ];
    }
}