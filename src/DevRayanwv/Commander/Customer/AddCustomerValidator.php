<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 6:39 PM
 */

namespace DevRayanwv\Commander\Customer;


class AddCustomerValidator {

    protected static $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:4'
    ];
    public function validator(AddCustomerCommand $command){
        $validator = \Validator::make([
            'name' => $command->name,
            'email' => $command->email,
            'password' => $command->password
        ], static::$rules);

        if($validator->fails())
        {
            die('Validation failed');
        }

    }
}