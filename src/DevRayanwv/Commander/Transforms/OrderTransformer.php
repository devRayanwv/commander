<?php namespace DevRayanwv\Commander\Transforms;
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 21/05/15
 * Time: 11:34 AM
 */


class OrderTransformer extends Transformer {
    public function transform($order){
        return [
            'id' => $order['id'],
            'userID' => $order['userID'],
            'total' => $order['total']
        ];
    }
}