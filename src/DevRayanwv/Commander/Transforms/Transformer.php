<?php namespace ADevRayanwv\Commander\Transforms;
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 21/05/15
 * Time: 11:31 AM
 */

abstract class Transformer {
    public function transformCollection($items){
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);
}