<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 6:28 PM
 */

namespace DevRayanwv\Commander\Commanding;


interface CommandBus {

    public function execute($command);
}