<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 11:05 AM
 */

namespace DevRayanwv\Commander\Commanding;

use Exception;

class CommandTranslator {

    public function toCommandHandler($command){
        $handler = str_replace('Command', 'CommandHandler', get_class($command));

        if (! class_exists($handler))
        {
            $message = "Command handler [$handler] does not exist.";
            throw new Exception($message);
        }

        return $handler;
    }

    public function toValidator($command){
        return   str_replace('Command', 'Validator', get_class($command));
    }
}