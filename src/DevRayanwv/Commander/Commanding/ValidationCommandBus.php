<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 6:24 PM
 */

namespace DevRayanwv\Commander\Commanding;


use Illuminate\Foundation\Application;

class ValidationCommandBus implements CommandBus {

    private $commandBus;
    private $app;
    private $commandTranslator;

    function __construct(DefaultCommandBus $commandBus, Application $app, CommandTranslator $commandTranslator)
    {
        $this->commandBus = $commandBus;
        $this->app = $app;
        $this->commandTranslator = $commandTranslator;

    }

    public function execute($command){

        $validator = $this->commandTranslator->toValidator($command);

        if (class_exists($validator))
        {
            $this->app->make($validator)->validator($command);
        }
        return $this->commandBus->execute($command);

    }
}