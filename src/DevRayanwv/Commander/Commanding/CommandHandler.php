<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 11:17 AM
 */

namespace DevRayanwv\Commander\Commanding;


interface CommandHandler {
    public function handle($command);
}