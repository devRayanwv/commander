<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 11:38 AM
 */

namespace DevRayanwv\Commander\Eventing;


trait EventGenerator {
    protected $pendingEvents = [];

    public function raise($event){
        $this->pendingEvents[] = $event;
    }

    public function releaseEvents(){
        $events = $this->pendingEvents;

        $this->pendingEvents = [];
        return $events;
    }
}