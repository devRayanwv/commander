<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 5:07 PM
 */

namespace DevRayanwv\Commander\Eventing;
use ReflectionClass;

class EventListener {

    public function handle($event){
        $eventName = $this->getEventName($event);

        if ($this->listenerIsRegistered($eventName))
        {
            return call_user_func([$this, 'when'.$eventName], $event);
        }
    }

    protected function getEventName($event){
        return (new ReflectionClass($event))->getShortName();
    }

    protected function listenerIsRegistered($eventName){
        $method = "when{$eventName}";
        return method_exists($this, $method);
    }
}