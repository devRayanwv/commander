<?php
/**
 * Created by PhpStorm.
 * User: rayan
 * Date: 27/05/15
 * Time: 5:25 PM
 */

namespace DevRayanwv\Commander\Eventing;


use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider {

    public function register(){
        $listeners = $this->app['config']->get('rayan.listeners');

        foreach ($listeners as $listener) {
            $this->app['events']->listen('App\Rayan\*', $listener);
        }
    }
}